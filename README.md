# IAM - ECS Policies

## Usage

```terraform
module "ecs_iam" {
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/iam-ecs-policies.git?ref=1.1"
}
```