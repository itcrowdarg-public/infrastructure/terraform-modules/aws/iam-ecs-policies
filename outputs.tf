output "aws_iam_role_cluster_service_role" {
  description = "IAM role for the cluster service"
  value       = aws_iam_role.cluster_service_role
}

output "aws_iam_policy_cluster_service_policy" {
  description = "cluster-service-policy-ecs-cluster"
  value       = aws_iam_policy.cluster_service_policy
}