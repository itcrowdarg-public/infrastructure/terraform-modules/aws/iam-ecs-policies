resource "aws_iam_role" "cluster_service_role" {
  description        = "cluster-service-role-ecs-cluster"
  assume_role_policy = file("${path.module}/policies/cluster-service-role.json")
}

resource "aws_iam_policy" "cluster_service_policy" {
  description = "cluster-service-policy-ecs-cluster"
  policy      = file("${path.module}/policies/cluster-service-policy.json")
}

resource "aws_iam_policy_attachment" "cluster_service_policy_attachment" {
  name       = "cluster-instance-policy-attachment-ecs-cluster"
  roles      = [aws_iam_role.cluster_service_role.id]
  policy_arn = aws_iam_policy.cluster_service_policy.arn
}
